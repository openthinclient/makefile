MAKEFILE_PATH:=$(lastword ${MAKEFILE_LIST})

# Includes project-specific variables
-include ${MAKEFILE_PATH:.mk=}.in

ifneq (${MAKECMDGOALS},init)  # Not for 'init'-target
 ifneq (${MAKECMDGOALS},help)  # Not for 'help'-target
  ifneq (${MAKECMDGOALS},)  # Not for default target
# Runs script before any target (for Ex., in order to auto-update original source before doing something else)
AUTORUN_BEFORE_ANY_TARGET?=./.autorun-before-any-target
_:=$(shell test -x ${AUTORUN_BEFORE_ANY_TARGET} && ${AUTORUN_BEFORE_ANY_TARGET} ${MAKECMDGOALS} >&2)
# Every regular file from source directories (':' in some filenames breaks targets, ignore them)
SRC=$(shell find */ -type f | grep -v ':' | sed 's/ /\\ /g')
PYTEST_OPTS?=
# To ignore some directory put into Makefile.in:
PYTEST_OPTS+=--doctest-modules

# Application name
NAME?=$(shell test -e debian/control && sed -n 's/^Source: *\(.*\)/\1/p' debian/control || basename $$(readlink -f .))
$(if ${NAME},,$(error Can not find or parse NAME))

BRANCH_TYPE?=$(shell (git symbolic-ref --short HEAD || echo HEAD) | sed "s/\/.*//")

VERSION?= $(shell head -1 debian/changelog | mawk '{print $$2}' | tr -d "()")
$(if ${VERSION},,$(error Can not find or parse VERSION))
   ifeq (${BRANCH_TYPE},master)  # releases
   else
   ifeq (${BRANCH_TYPE},support) # support releases
   else
# BUILD_ID is set by CI or empty
VERSION:=${VERSION}~${BRANCH_TYPE}${BUILD_ID}
   endif
   endif

PACKAGE_NAME=${NAME}_${VERSION}

# Default values (can be set as environmental variables)
TARGET_ARCH?=amd64
#
LOGIN?=
DEFAULT_UPLOAD_PACKAGE_LOGIN?=$(shell whoami)
DEFAULT_INSTALL_LOGIN?=openthinclient
DEFAULT_INSTALL_TO_CLIENT_LOGIN=tcos
#
PASSWORD?=
DEFAULT_INSTALL_PASSWORD?=0pen%TC
DEFAULT_INSTALL_TO_CLIENT_PASSWORD=0pen%TC
#
BASE_PATH?=
DEFAULT_UPLOAD_PACKAGE_PATH?=/var/www/openthinclient/manager-
DEFAULT_INSTALL_PATH?=/home/openthinclient/otc-manager-home/nfs/root
DEFAULT_INSTALL_TO_CLIENT_PATH?=/
#
TAG?=
DEFAULT_TAG?=rolling
#
CP_OPTS?=-r --preserve=links  # Can be replaced with '-al', but be careful with side-effects
MKSQUASHFS_OPTS?=-comp lzo -Xcompression-level 9 -noappend -always-use-fragments -action 'guid(1000, 1000) @ name(*)' -action 'exclude @ name(.empty) && filesize(0)'
ifneq ("$(wildcard mksquashfs.actions)","")
  MKSQUASHFS_OPTS+=-action-file mksquashfs.actions
endif
CHROOT_LINUX_VERSION?=buster
CHROOT_LINUX_URL?=http://ftp.de.debian.org/debian
CHROOT_PATH?=.build/.chroot-debian-${CHROOT_LINUX_VERSION}-${TARGET_ARCH}
CREATE_CHROOT?=

# Build options
COMPATIBILITY_LEVEL=13
export DH_VERBOSE?=1
export CFLAGS?=-Wall -g -O2
export DEB_HOST_ARCH?=${TARGET_ARCH}

# Command line arguments for check-tools
CHECK_PYTHON_ARGS?=-q

DEB_TARGETS= \
			 .build/${PACKAGE_NAME}_${TARGET_ARCH}.deb \
			 .build/${PACKAGE_NAME}_${TARGET_ARCH}.deb.md5 \
			 .build/${PACKAGE_NAME}_${TARGET_ARCH}.changes \
			 .build/${NAME}.changelog \

   ifneq (${WITH_SRC:no=},)
	ifeq ($(shell find */ -iname "worker.pyx" -or -iname "tcoslc.pyx" -or -iname "_launcher.pyx"),)  # For non-licensed packages
DEB_TARGETS+= \
			 .build/${PACKAGE_NAME}.tar.gz \

	endif
   endif

  endif
 endif
endif


# Disables built-in rules for every suffix
.SUFFIXES:


all: help


## Creates an empty package structure.
##
## Usage:
##     1) Run make init NAME=... VERSION=...
##     2) Create symlink from Makefile into <package>/
##     3) Edit <package>/debian/control
##     3) Edit <package>/debian/changelog
ifndef OVERRIDE_INIT_TARGET
init::
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	@$(if ${DEBFULLNAME},,$(eval DEBFULLNAME=$(shell read -p "Enter developer's full name: " RESULT && echo $${RESULT})))
	@$(if ${DEBFULLNAME},,$(error Empty developer\'s full name))

	@$(if ${DEBEMAIL},,$(eval DEBEMAIL=$(shell read -p "Enter developer's email: " RESULT && echo $${RESULT})))
	@$(if ${DEBEMAIL},,$(error Empty developer\'s email))

	@(test -e ${NAME} \
		&& echo Directory \'${NAME}\' already exists && echo \
		&& exit 1 \
		|| ( \
			mkdir -p ${NAME}-${VERSION} \
			&& ( \
				cd ${NAME}-${VERSION} \
				&& DEBFULLNAME="${DEBFULLNAME}" DEBEMAIL="${DEBEMAIL}" \
					dh_make --single --yes --createorig \
			) \
			&& rm -f ${NAME}_${VERSION}.orig.* \
			&& mv ${NAME}-${VERSION} ${NAME} \
			&& find ${NAME}/debian/* -not -name control | xargs -r rm -rf \
			&& ( \
				cd ${NAME} \
				&& DEBFULLNAME="${DEBFULLNAME}" DEBEMAIL="${DEBEMAIL}" dch --create -v ${VERSION} \
				) \
			&& install -d \
				${NAME}/package-rootfs/opt/${NAME} \
				${NAME}/schema/application \
				${NAME}/package-metadata \
				${NAME}/tcos \
				${NAME}/gui-checks \
			&& touch ${NAME}/package-rootfs/opt/${NAME}/.empty \
			&& touch ${NAME}/schema/application/${NAME}.xml \
			&& touch ${NAME}/package-metadata/${NAME}.meta.xml \
		) \
	)

	@echo "Done."
endif


## Builds a debian and python packages.
##
## Customizable environment variables:
##     TARGET_ARCH -- default: amd64
##     CP_OPTS -- be careful with side-effects if you set it to "-al", default: "-r"
##     CHROOT_PATH -- path, where to look for/download a linux (environment to compile *.pyx)
##     CREATE_CHROOT -- (bool) force to create chroot (do not ask for)
##     CHROOT_LINUX_URL -- URL where to find a linux environment (for debootstrap)
##     CHROOT_LINUX_VERSION -- linux version (for debootstrap)
ifndef OVERRIDE_BUILD_TARGET
build:: \
	.build/${PACKAGE_NAME}_${TARGET_ARCH}.deb \

#build: \
#    .build/${PACKAGE_NAME}_${TARGET_ARCH}.whl \

endif


## Uploads everything needed into repository.
##
## Usage:
##     make upload-package DST=[user:[password]@]host[:(path|tag)]
##
## Customizable environment variables:
##     DST -- required, format: [user:[password]@]host[:(path|tag)]
##     LOGIN -- default: ${DEFAULT_UPLOAD_PACKAGE_LOGIN}
##     PASSWORD -- default: use appropriate certificate or ask for a password
##     BASE_PATH -- default: ${DEFAULT_UPLOAD_PACKAGE_PATH}
##     TAG -- default: ${DEFAULT_TAG}
##
## Use it to publish the package on the test/deploy server.
ifndef OVERRIDE_UPLOAD_PACKAGE_TARGET
upload-package:: \
		${DEB_TARGETS} \
		| .parse-dst-argument .check-free-space-in-repository

	@echo "\n========================================" && echo "Target \`$@\`:\n"

	@# Uploads files to HOST and installs them into BASE_PATH+TAG
	( \
		cd $(dir $<) \
		&& ( \
			tar czf - $(notdir $^) \
			| $(if ${PASSWORD},sshpass -p ${PASSWORD} )ssh $(or ${LOGIN},${DEFAULT_UPLOAD_PACKAGE_LOGIN})@${HOST} \( \
				cd $(or ${BASE_PATH},${DEFAULT_UPLOAD_PACKAGE_PATH}$(or ${TAG},${DEFAULT_TAG}))/ \
				\&\& sudo tar xvzf - --overwrite \
			\) 1>&- \
			|| ssh $(or ${LOGIN},${DEFAULT_UPLOAD_PACKAGE_LOGIN})@${HOST} \
		) \
	)

	@# Re-creates meta-data (Packages/Release/Sources) in repository
	$(MAKE) -f ${MAKEFILE_PATH} update-repository

	@echo "Done."
endif


## Re-creates meta-data (Packages/Release/Sources) in repository.
##
## Usage:
##     make update-repository DST=[user:[password]@]host[:(path|tag)]
##
## Customizable environment variables:
##     DST -- required, format: [user:[password]@]host[:(path|tag)]
##     LOGIN -- default: ${DEFAULT_UPLOAD_PACKAGE_LOGIN}
##     PASSWORD -- default: use appropriate certificate or ask for a password
##     BASE_PATH -- default: ${DEFAULT_UPLOAD_PACKAGE_PATH}
##     TAG -- default: ${DEFAULT_TAG}
##
## Use it to update repository meta-data if package has been moved/uploaded manually.
ifndef OVERRIDE_UPDATE_REPOSITORY_TARGET
update-repository:: \
		| .parse-dst-argument
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	@# Re-creates meta-data (Packages/Release/Sources) in repository
	@$(if ${PASSWORD},sshpass -p ${PASSWORD} )ssh $(or ${LOGIN},${DEFAULT_UPLOAD_PACKAGE_LOGIN})@${HOST} "sudo bash -c \
		\"cd $(or ${BASE_PATH},${DEFAULT_UPLOAD_PACKAGE_PATH}$(or ${TAG},${DEFAULT_TAG})) \
			&& (echo -n Running apt-ftparchive packages... && apt-ftparchive packages . > Packages && echo done) \
			&& (echo -n Compressing Packages with gz... && gzip -9cf < Packages > Packages.gz && echo done) \
			&& (echo -n Compressing Packages with bz2... && bzip2 -9cf < Packages > Packages.bz2 && echo done) \
			&& (echo -n Running apt-ftparchive sources... && apt-ftparchive sources . > Sources && echo done) \
			&& (echo -n Compressing Sources with gz... && gzip -9cf < Sources > Sources.gz && echo done) \
			&& (echo -n Compressing Sources with bz2... && bzip2 -9cf < Sources > Sources.bz2 && echo done) \
			&& (echo -n Running apt-ftparchive release... && apt-ftparchive release . > Release && echo done) \
			&& (echo -n Changing mode... && chmod 444 * && echo done) \
			&& (echo -n Changing user/group... && chown www-data.www-data * && echo done) \
		\" \
	"

	@echo "Done."
endif


## Replaces data of already installed package on the server.
##
## Usage:
##     make install DST=[user:[password]@]host[:path]
##
## Customizable environment variables:
##     DST -- required, format: [user:[password]@]host[:path]
##     LOGIN -- default: ${DEFAULT_INSTALL_LOGIN}
##     PASSWORD -- default: use appropriate certificate or ask for a password
##     BASE_PATH -- default: ${DEFAULT_INSTALL_PATH}
##
## Use it for testing purpose to avoid a full reinstall of the package.
ifndef OVERRIDE_INSTALL_TARGET
install:: \
		.build/${PACKAGE_NAME}_${TARGET_ARCH}.deb \
		| .parse-dst-argument
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	@# Uploads files to HOST and installs them into BASE_PATH
	$(eval INSTALL_SRC=$(shell ls .build/${PACKAGE_NAME}/debian-package/debian/${NAME} | grep -v DEBIAN))
	( \
		cd .build/${PACKAGE_NAME}/debian-package/debian/${NAME} \
		&& ( \
			tar czf - ${INSTALL_SRC} \
			| $(if $(or ${PASSWORD},${DEFAULT_INSTALL_PASSWORD}),sshpass -p $(or ${PASSWORD},${DEFAULT_INSTALL_PASSWORD}) )ssh -oStrictHostKeyChecking=no $(or ${LOGIN},${DEFAULT_INSTALL_LOGIN})@${HOST} \( \
				set +x \; \
				sudo mkdir -p $(or ${BASE_PATH},${DEFAULT_INSTALL_PATH})/ \; \
				[[ "$(or ${BASE_PATH},${DEFAULT_INSTALL_PATH})" == "/tcos/link*" ]] \&\& sudo mount -o remount,rw /tcos/link \|\| true \; \
				cd $(or ${BASE_PATH},${DEFAULT_INSTALL_PATH})/ \; \
				sudo tar xvzf - --overwrite \; \
				echo "Terminated with exit code $$?" \; \
				sudo sync \; \
				[[ "$(or ${BASE_PATH},${DEFAULT_INSTALL_PATH})" == "/tcos/link*" ]] \&\& sudo mount -o remount,rw /tcos/link \|\| true \; \
			\) 1>&1 \
		) \
	)
	@# twine upload --repository-url http://localhost:8080/ ./.build/rdesktop-2018.1-py2-none-any.whl

	@echo "Done."
endif


## Replaces data of already installed package on the client.
##
## Usage:
##     make install-to-client DST=[user:[password]@]host[:path]
##
## Customizable environment variables:
##     DST -- required, format: [user:[password]@]host[:path]
##     LOGIN -- default: ${DEFAULT_INSTALL_TO_CLIENT_LOGIN}
##     PASSWORD -- default: use appropriate certificate or ask for a password
##     BASE_PATH -- default: ${DEFAULT_INSTALL_TO_CLIENT_PATH}
##
## Use it for testing purpose to avoid a full reinstall of the package and reboot of client.
ifndef OVERRIDE_INSTALL_TO_CLIENT_TARGET
install-to-client:: \
		.build/${PACKAGE_NAME}/${NAME}.sfs \
		| .parse-dst-argument
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	@# Uploads files to HOST and installs them into BASE_PATH
	$(eval INSTALL_SRC=$(shell ls .build/${PACKAGE_NAME}/squashfs-data | grep -v DEBIAN))
	( \
		cd .build/${PACKAGE_NAME}/squashfs-data \
		&& ( \
			tar czf - ${INSTALL_SRC} \
			| $(if $(or ${PASSWORD},${DEFAULT_INSTALL_TO_CLIENT_PASSWORD}),sshpass -p $(or ${PASSWORD},${DEFAULT_INSTALL_TO_CLIENT_PASSWORD}) )ssh -oStrictHostKeyChecking=no $(or ${LOGIN},${DEFAULT_INSTALL_TO_CLIENT_LOGIN})@${HOST} \( \
				set +x \; \
				sudo mkdir -p $(or ${BASE_PATH},${DEFAULT_INSTALL_TO_CLIENT_PATH})/ \; \
				[[ "$(or ${BASE_PATH},${DEFAULT_INSTALL_TO_CLIENT_PATH})" == "/tcos/link*" ]] \&\& sudo mount -o remount,rw /tcos/link \|\| true \; \
				cd $(or ${BASE_PATH},${DEFAULT_INSTALL_TO_CLIENT_PATH})/ \; \
				sudo tar xvzf - --keep-newer-files \; \
				echo "Terminated with exit code $$?" \; \
				[[ "$(or ${BASE_PATH},${DEFAULT_INSTALL_TO_CLIENT_PATH})" == "/tcos/link*" ]] \&\& sudo mount -o remount,ro /tcos/link \|\| true \; \
			\) 1>&- \
		) \
	)

	@echo "Done."
endif


# Builds a debian-package (+ sources in compressed tarball)
${DEB_TARGETS}: \
		${SRC} \
		.build/${PACKAGE_NAME}/${NAME}.sfs \
		| .build/${PACKAGE_NAME}/debian-package
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	rm -rf ".build/${PACKAGE_NAME}/debian-package/*" $@

	@# Compresses sources (together with Makefile (dereferenced link))
	mkdir -p .build/${PACKAGE_NAME}/debian-package/debian
	test -e debian/changelog && ( \
		echo "Copying existing debian/changelog..."; \
		cp ${CP_OPTS} debian/changelog .build/${PACKAGE_NAME}/debian-package/debian/;\
	) || ( \
		echo "Generating changelog from repository log..."; \
		$(MAKE) -f ${MAKEFILE_PATH} .build/${PACKAGE_NAME}/debian-package/debian/changelog; \
	)
ifeq (${BRANCH_TYPE},master)  # For master-branch
else
	awk -v VERSION=${VERSION} 'NR==1{ $$2 = "("VERSION")" } { print }'  < debian/changelog > .build/${PACKAGE_NAME}/debian-package/debian/changelog
 endif
	@#@echo ================================
	@#@cat .build/${PACKAGE_NAME}/debian-package/debian/changelog
	@#@echo ================================
	cp ${CP_OPTS} debian/control .build/${PACKAGE_NAME}/debian-package/debian/
	#cp ${CP_OPTS} --dereference ${MAKEFILE_PATH} .build/${PACKAGE_NAME}/debian-package/
	for PACKAGE_ROOTFS_PATH in $(sort $(wildcard package-rootfs*)); do \
		(test ! -d $${PACKAGE_ROOTFS_PATH} && exit 0 \
			|| cp ${CP_OPTS} $${PACKAGE_ROOTFS_PATH} .build/${PACKAGE_NAME}/debian-package/ \
		); \
	done
	(test ! -d tcos && exit 0 \
		|| cp ${CP_OPTS} tcos .build/${PACKAGE_NAME}/debian-package/ \
	)
	(test ! -d gui-checks && exit 0 \
		|| cp ${CP_OPTS} gui-checks .build/${PACKAGE_NAME}/debian-package/ \
	)
	(test ! -d schema && exit 0 \
		|| cp ${CP_OPTS} schema .build/${PACKAGE_NAME}/debian-package/ \
	)
	(test ! -d package-metadata && exit 0 \
		|| cp ${CP_OPTS} package-metadata .build/${PACKAGE_NAME}/debian-package/ \
	)
	(test ! -d custom && exit 0 \
		|| cp ${CP_OPTS} custom .build/${PACKAGE_NAME}/debian-package/ \
	)
	(test -z "$(filter %.tar.gz,${DEB_TARGETS})" && exit 0 \
		|| cd .build/ && dpkg-source -b ${PACKAGE_NAME}/debian-package \
	)

	mkdir -p .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/

ifneq (,$(wildcard package-rootfs/. tcos/. gui-checks/.))  # Directories package-rootfs/, tcos/ or gui-checks/ exist
endif
	@# Copies squashfs-file and changelog
	mkdir -p .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/sfs/package/
	cp ${CP_OPTS} .build/${PACKAGE_NAME}/${NAME}.sfs .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/sfs/package/
	cp ${CP_OPTS} .build/${PACKAGE_NAME}/debian-package/debian/changelog .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/sfs/package/${NAME}.changelog

	@# Copies schema/ (if schema/ exists)
	(test ! -d schema && exit 0 \
		|| cp ${CP_OPTS} schema .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/ \
	)

	@# Copies package-metadata/ (if package-metadata/ exists)
	(test ! -d package-metadata && exit 0 \
		|| cp ${CP_OPTS} package-metadata .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/ \
	)

	@# Copies custom/ (if custom/ exists)
	(test ! -d custom && exit 0 \
		|| cp ${CP_OPTS} custom .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/ \
	)

	@# Creates list of files with theirs MD5-sums
	cd .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/sfs/package/ && find ../.. -type f -not -iname "*.md5" | xargs realpath --relative-to . | xargs md5sum | tee ${NAME}.md5

	@# Creates zsync file for squashfs-file
	mkdir -p .build/${PACKAGE_NAME}/debian-package/debian/${NAME}
	zsyncmake  .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/sfs/package/${NAME}.sfs -o .build/${PACKAGE_NAME}/debian-package/debian/${NAME}/sfs/package/${NAME}.sfs.zsync -u ${NAME}.sfs

	@# Builds deb-package for ${TARGET_ARCH}-architecture
	echo "${COMPATIBILITY_LEVEL}" > .build/${PACKAGE_NAME}/debian-package/debian/compat
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_testdir  # debian/control needed
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_testroot
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_link  # debian/compat needed
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_strip
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_compress
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_fixperms
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_installdeb
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_gencontrol  # debian/changelog needed
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_md5sums
	cd .build/${PACKAGE_NAME}/debian-package/ && fakeroot dh_builddeb -- -Zgzip
	cp ${CP_OPTS} .build/${PACKAGE_NAME}/${PACKAGE_NAME}_${TARGET_ARCH}.deb .build/

	@# Generates md5-sum of the deb-package
	cd .build && md5sum ${PACKAGE_NAME}_${TARGET_ARCH}.deb > ${PACKAGE_NAME}_${TARGET_ARCH}.deb.md5

	@# Copies change-log
	cp ${CP_OPTS} .build/${PACKAGE_NAME}/debian-package/debian/changelog ".build/${NAME}.changelog"

	@# Generates .changes-file
	(test -z "$(filter %.tar.gz,${DEB_TARGETS})" && exit 0 \
		|| cp ${CP_OPTS} \
			.build/${PACKAGE_NAME}.tar.gz \
				.build/${PACKAGE_NAME}/ \
	)
	cp ${CP_OPTS} .build/${PACKAGE_NAME}_${TARGET_ARCH}.deb .build/${PACKAGE_NAME}/
	cd .build/${PACKAGE_NAME}/debian-package/ && dpkg-genchanges -q $(if $(filter %.tar.gz,${DEB_TARGETS}),,-b) > ../../${PACKAGE_NAME}_${TARGET_ARCH}.changes

	@echo "Done."


# Builds a python wheel package
.build/${PACKAGE_NAME}_${TARGET_ARCH}.whl: \
		${SRC} \
		.build/${PACKAGE_NAME}/${NAME}.sfs \
		| .build/${PACKAGE_NAME}/python-package
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	rm -rf ".build/${PACKAGE_NAME}/python-package/*" $@

	mkdir -p .build/${PACKAGE_NAME}/python-package/

ifneq (,$(wildcard package-rootfs/. tcos/. gui-checks/.))  # Directories package-rootfs/, tcos/ or gui-checks/ exist
	@# Copies squashfs-file and changelog
	mkdir -p .build/${PACKAGE_NAME}/python-package/sfs/package/
	cp ${CP_OPTS} .build/${PACKAGE_NAME}/${NAME}.sfs .build/${PACKAGE_NAME}/python-package/sfs/package/
	cp ${CP_OPTS} debian/changelog .build/${PACKAGE_NAME}/python-package/sfs/package/${NAME}.changelog
endif

	@# Copies schema/ (if schema/ exists)
	(test ! -d schema && exit 0 \
		|| cp ${CP_OPTS} schema .build/${PACKAGE_NAME}/python-package/ \
	)

	@# Copies package-metadata/ (if package-metadata/ exists)
	(test ! -d package-metadata && exit 0 \
		|| cp ${CP_OPTS} package-metadata .build/${PACKAGE_NAME}/python-package/ \
	)

	@# Copies custom/ (if custom/ exists)
	(test ! -d custom && exit 0 \
		|| cp ${CP_OPTS} custom .build/${PACKAGE_NAME}/python-package/ \
	)

	@# Composes setup.py
	@> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "#!/usr/bin/env python" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "import os" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "import setuptools" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "kwargs = dict(" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    name='${NAME}'," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    version='${VERSION}'," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    # author='$(shell git log -1 --pretty="%an")'," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    # author_email='$(shell git log -1 --pretty="%ae")'," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    # package_dir={'schema': 'schema'}," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    # package_data={'schema': [os.path.join(path, x) for path, directories, files in os.walk('schema') for x in files]}," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    # package_dir={'schema': 'schema'}," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    # package_data={'schema': ['application/rdesktop.xml']}," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    # data_files=[['schema', ['schema/application/rdesktop.xml']]]," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "    data_files=[[path, [os.path.join(path, x) for x in files]] for path, directories, files in os.walk('./') if path != './' and not path.startswith('./dist') and not path.startswith('./$(subst -,_,${NAME}).egg-info') and files]," >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo ")" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@#@echo "print 'setup('" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@#@echo "print kwargs" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@#@echo "print ')'" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@echo "setuptools.setup(**kwargs)" >> .build/${PACKAGE_NAME}/python-package/setup.py
	@#@echo ================================
	@#@cat .build/${PACKAGE_NAME}/python-package/setup.py
	@#@echo ================================
	chmod a+x .build/${PACKAGE_NAME}/python-package/setup.py

	@# Builds python-package for ${TARGET_ARCH}-architecture
	cd .build/${PACKAGE_NAME}/python-package/ && ./setup.py bdist_wheel
	cp ${CP_OPTS} .build/${PACKAGE_NAME}/python-package/dist/*.whl $@

	@echo "Done."


# Creates squashfs-file
.build/${PACKAGE_NAME}/${NAME}.sfs: \
		${SRC} \
		| .build/${PACKAGE_NAME}/squashfs-data
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	rm -rf ".build/${PACKAGE_NAME}/squashfs-data/*"

	@# Copies package-rootfs*/ into hierarchy (if package-rootfs*/ exist)
	for PACKAGE_ROOTFS_PATH in $(sort $(wildcard package-rootfs*)); do \
		(test ! -d $${PACKAGE_ROOTFS_PATH} && exit 0 \
			|| cp ${CP_OPTS} $${PACKAGE_ROOTFS_PATH}/. .build/${PACKAGE_NAME}/squashfs-data/ \
		); \
	done
	@# Copies tcos/ into hierarchy (if tcos/ exists)
	(test ! -d tcos && exit 0 \
		|| mkdir -p .build/${PACKAGE_NAME}/squashfs-data/opt/${NAME}/ \
		&& cp ${CP_OPTS} tcos .build/${PACKAGE_NAME}/squashfs-data/opt/${NAME}/ \
	)
	@# Copies schema/ into hierarchy (if schema/ exists)
	(test ! -d schema && exit 0 \
		|| mkdir -p .build/${PACKAGE_NAME}/squashfs-data/tcos/schema/ \
		&& cp ${CP_OPTS} schema/. .build/${PACKAGE_NAME}/squashfs-data/tcos/schema \
	)
	@# Copies changelog into hierarchy
	(mkdir -p .build/${PACKAGE_NAME}/squashfs-data/tcos/changelogs/ \
		&& cp ${CP_OPTS} debian/changelog .build/${PACKAGE_NAME}/squashfs-data/tcos/changelogs/${PACKAGE_NAME}.changelog \
	)
	@# Copies package-metadata/ into hierarchy (if package-metadata/ exists)
	(test ! -d package-metadata && exit 0 \
		|| mkdir -p .build/${PACKAGE_NAME}/squashfs-data/tcos/package-metadata/ \
		&& cp ${CP_OPTS} package-metadata/. .build/${PACKAGE_NAME}/squashfs-data/tcos/package-metadata \
	)
	@# Copies gui-checks/ into hierarchy (if gui-checks/ exists)
	(test ! -d gui-checks && exit 0 \
		|| mkdir -p .build/${PACKAGE_NAME}/squashfs-data/opt/${NAME}/ \
		&& cp ${CP_OPTS} gui-checks .build/${PACKAGE_NAME}/squashfs-data/opt/${NAME}/ \
	)
	@# Builds every directory with setup.py in a chroot-environment and removes sources
	for FILENAME in $$(find .build/${PACKAGE_NAME}/squashfs-data/ -iname 'setup.py'); do \
		$(MAKE) -f ${MAKEFILE_PATH} $${FILENAME}.done || exit $$?; \
	done
	find .build/${PACKAGE_NAME}/squashfs-data/ -iname '*.pyx' -or -iname 'setup.py' | xargs -r rm

	@# Generates squashfs-file and copies changelog nearby
	mksquashfs .build/${PACKAGE_NAME}/squashfs-data/ $@ ${MKSQUASHFS_OPTS}

	@echo "Done."


%/changelog:
	$(MAKE) -f ${MAKEFILE_PATH} generate-changelog DST=$@


## Generates debian/changelog from repository history
##
## Usage:
##     Commit messages selected for changelog must be written with a leading *
##       For example:
##         * Added feature xxx
##       or:
##         * Added feature xxx
##         * Fixed bug yyy * Added feature zzz
##     Tags for changelog must contain only version number
##       For example:
##         tag: 2018.6
##
## Customizable environment variables:
##     DST -- optional, where to save generated data instead of printing them out. Format: path
##
## Attention! If file "debian/changelog-prev" found, it will be added to the end of generated data!
ifndef OVERRIDE_GENERATE_CHANGELOG_TARGET
generate-changelog::
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	$(eval DEVELOPER_NAME=Development Team)
	$(eval DEVELOPER_EMAIL=devteam@openthinclient.com)

	@# Generates debian/changelog from repository history (from "tag: changelog-generator" if exists, otherwise from the beginning)
	@# Create this tag with:
	@#   > git tag -f changelog-generator <tag_or_hash>
	@git log --date-order --format='|TAGS=%D|SIGNATURE=%cD%nMESSAGE=%s' $(if $(shell git tag -l changelog-generator),changelog-generator..,) \
		| sed -e 's/^|TAGS=[^|]*tag: \([0-9.-]\+\)[^|]*/|TAG=\1/'  $(# Parses tags with version number and marks them) \
		| grep -v '^|TAGS='  $(# Ignores other tags) \
		| sed -e 's/^MESSAGE= */|MESSAGE=/'  $(# Marks messages) \
		| sed '/^|MESSAGE=[^\*]/d'  $(# Filters out messages without leading *) \
		| sed '/./{H;$$!d}; x; s/\n\(|MESSAGE=\)/\1/g'  $(# Joins multiple messages into one line leading with tags and signatures) \
		| sed 's/^|MESSAGE=/|TAG=${VERSION}|SIGNATURE=$(shell date --rfc-2822)\0/'  $(# Inserts current version for not tagged messages) \
		| sed 's/\(|SIGNATURE=[^|]\+\)\(.*\)/\2\1/'  $(# Moves signatures to the end of lines) \
		\
		| sed 's/^|TAG=\([^|]\+\)/${NAME} (\1) unstable; urgency=low\n\n/'  $(# Generates headers from sub-strings containing |TAG=)\
		| sed '/|MESSAGE=/{ s/ \+\* /\n  * /g; s/|MESSAGE=\([^|]\+\)/  \1\n/g; }'  $(# Generates messages from sub-strings containing |MESSAGE=) \
		| sed 's/^|SIGNATURE=\([^|]\+\)/\n -- ${DEVELOPER_NAME} <${DEVELOPER_EMAIL}>  \1\n/'  $(# Generates footers from sub-strings containing |SIGNATURE=) \
		| sed '1{/^$$/d}'  $(# Skips first empty line) \
			$(if ${DST},> ${DST},)

	@# Appends previous (hard-coded) changelog
	@test ! -e debian/changelog-prev && true || ( \
		cat debian/changelog-prev $(if ${DST},>> ${DST},); \
	)

	@echo "Done."
endif


# Compiles a cython-source
%.py.done: ${CHROOT_PATH}
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	@# Compiles source in CHROOT_PATH
	cd ${CHROOT_PATH}/tmp/ && ls -A | xargs sudo rm -rf
	mkdir \
		${CHROOT_PATH}/tmp/.src \
		${CHROOT_PATH}/tmp/.build \
	;
	cp -r $(dir $@)/. ${CHROOT_PATH}/tmp/.src/
	sudo chroot ${CHROOT_PATH} /bin/sh -c " \
		apt install -y --force-yes \
			python2.7-minimal \
			python2.7-dev \
			python-pip \
		; \
		pip install \
			cython \
		; \
		(cd /tmp/.src \
			&& python2.7 setup.py build --build-base=/tmp/.build \
		); \
	"
	cp -r ${CHROOT_PATH}/tmp/.build/lib*/*.so $(dir $@)/

	@echo "Done."


## Updates nested application from its source.
##
## Customizable environment variables:
##     SRC_URL | SRC_PATH | SRC_GIT_URL -- required, url/path to the source file
##     SRC_GIT_COMMIT -- optional, defaults to master
##     SRC_DEPS -- optional, list of dependencies for "apt"
##     SRC_CONFIGURE -- optional, commands to configure
##     SRC_MAKE -- optional, commands to make
##     SRC_MAKE_INSTALL -- optional, commands to install ("sudo" is vorbidden)
##
## Use Makefile.in to store these variables.
ifndef OVERRIDE_UPDATE_FROM_SRC_TARGET
update-from-src:: ${CHROOT_PATH}
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	mkdir -p .build/.src

	@# Downloads original source from SRC_URL
	$(if ${SRC_URL},(true \
		&& cd .build \
		&& (test -f .src.tgz && exit 0 || wget -c ${SRC_URL} --output-document=.src.tgz) \
	))
	@# or copies original source from SRC_PATH
	$(if ${SRC_PATH},(true \
		&& cp ${SRC_PATH} .build/.src.tgz \
	))
	@# and extracts original source
	$(if $(shell ls -A .build/.src.tgz),(true \
		&& cd .build \
		&& tar xzvf .src.tgz --directory=.src \
	))
	@# or clones git repo
	$(if ${SRC_GIT_URL},(true \
		&& git clone ${SRC_GIT_URL} .build/.src \
		|| (cd .build/.src && git pull) \
		&& cd .build/.src \
		&& git checkout $(or ${SRC_GIT_COMMIT}, master) \
	))

	@# Cleans paths (removes also hidden entries)
	-sudo umount -f ${CHROOT_PATH}/tmp/.src
	cd ${CHROOT_PATH}/tmp/ && ls -A | xargs sudo rm -rf
	cd ${CHROOT_PATH}/var/tmp/ && ls -A | xargs sudo rm -rf
	@# Mounts instead of make a copy in order to save intermediate data (*.o) between builds (saves building time)
	mkdir ${CHROOT_PATH}/tmp/.src
	test -z ".build/.src" && exit 0 || sudo mount --bind .build/.src ${CHROOT_PATH}/tmp/.src
	@# Compiles source in CHROOT_PATH
	sudo chroot ${CHROOT_PATH} /bin/sh -c " \
		uname -a; \
		echo '----------------------------------------' && echo 'INSTALLING DEPENDENCIES...'; \
		apt update; apt install -y --force-yes \
			checkinstall \
			sudo \
			rpm \
			${SRC_DEPS} \
			; \
		(cd /tmp/.src \
			&& echo '----------------------------------------' && echo 'RUNNING CONFIGURE/CMAKE...' \
			&& ($(or ${SRC_CONFIGURE}, true)) \
			&& echo '----------------------------------------' && echo 'RUNNING MAKE...' \
			&& find . \
			&& ($(or ${SRC_MAKE}, true)) \
			&& echo '----------------------------------------' && echo 'RUNNING CHECKINSTALL...' \
			&& rm -rf .package /var/tmp/* \
			&& mkdir -p .package \
			&& fakeroot checkinstall \
				--nodoc \
				-y \
				--pakdir=./ \
				--install=no \
				-d 2 \
					$(or ${SRC_MAKE_INSTALL}, make install) \
			&& cp -r /var/tmp/tmp.*/package/. .package \
		); \
	"
	@# Cleans chroot environment
	-sudo umount -f ${CHROOT_PATH}/tmp/.src

	# Moves previous "package-rootfs" directory into .package-rootfs-<date> (if exists)
	@if test -d package-rootfs; then \
		mv -T package-rootfs .package-rootfs-$$(date +%F) \
			|| ( \
				>&2 echo "" \
				&& >&2 echo "ERROR: Can not rename \"package-rootfs\" to \".package-rootfs-$$(date +%F)\"! Destination already exists!" \
				&& >&2 echo "" \
				&& exit 1 \
			); \
	fi

	# Moves new data into "package-rootfs" (with access to current user) and removes Files
	sudo chown -R $${USER}:$${USER} .build/.src/.package
	mv .build/.src/.package package-rootfs && (cd package-rootfs && rm -rf ${SRC_REMOVE_FILES})
	@echo "Done."
endif

# Checks if all knowledge base articles exist
check-kbarticles:
	$(eval KBARTICLES := $(shell grep -R 'kbarticle="[^ "]*' schema -oh | sed 's/kbarticle="//'))
	$(eval KBVERSION := $(shell echo ${VERSION} | cut -c1-6 | tr -d .))
	$(eval URL := $(shell echo https://wiki.openthinclient.org/display/_PK/OMD$(KBVERSION)/))
	for ARTICLE in $(KBARTICLES) ; do \
		echo -n Checking knowledge base article $$ARTICLE for version $(KBVERSION); \
		if ! curl -s --head  --request GET ${URL}$$ARTICLE | grep "HTTP.*404" > /dev/null; \
		then \
		   echo " ✓" ; \
		else \
		   echo " NOT FOUND ✗" ; \
		   NOTFOUND=true; \
		fi \
	done; \
	if [ "$$NOTFOUND" = true ] ; then \
		echo "At least one knowledge base article returned 404."; \
		exit 1; \
	fi;
	@echo "Done."

# Downloads a linux-environment into ${CHROOT_PATH} if user accepted it
${CHROOT_PATH}:
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	@$(if ${CREATE_CHROOT},RESULT=y,read -p "Directory '$@' does not exist.  Would you like to  1. create it  and  2. download a linux base  into it? [y/N] " RESULT) \
		&& test "$${RESULT}" != "y" && exit 1 \
		|| sudo debootstrap --arch ${TARGET_ARCH} --verbose ${CHROOT_LINUX_VERSION} ${CHROOT_PATH} ${CHROOT_LINUX_URL} \

	@echo "Done."


# Creates directories
.build:
.build/${PACKAGE_NAME}: .build
.build/${PACKAGE_NAME}/squashfs-data: .build/${PACKAGE_NAME}
.build/${PACKAGE_NAME}/debian-package: .build/${PACKAGE_NAME}
.build/${PACKAGE_NAME}/python-package: .build/${PACKAGE_NAME}
.build \
.build/${PACKAGE_NAME} \
.build/${PACKAGE_NAME}/squashfs-data \
.build/${PACKAGE_NAME}/debian-package \
.build/${PACKAGE_NAME}/python-package \
		:
	@mkdir -p $@


.parse-dst-argument:
ifndef DST
	@echo
	@echo "ERROR: argument DST is undefined"
	@echo
	@echo "USAGE: make ${MAKECMDGOALS} DST=[user[:password]@](hostname|ip)[:(path|tag)]"
	@echo
	@exit 1
else
	@# Parsing HOST from DST
	$(eval HOST=$(shell echo "${DST}" | sed 's/.*@//' | sed 's/:.*//'))
	@# Parsing LOGIN and PASSWORD from DST
 ifneq (,$(findstring @,"${DST}"))  # '@' is found
	$(eval LOGIN=$(shell echo "${DST}" | sed 's/@.*//' | sed 's/:.*//'))
  ifneq (,$(findstring :,"$(shell echo "${DST}" | sed 's/@.*//')"))  # ':' before '@' is found
	$(eval PASSWORD=$(shell echo "${DST}" | sed 's/@.*//' | sed 's/.*://'))
  endif
 endif
	@# Parsing BASE_PATH from DST (if 'install'-target was found)
 ifneq (,$(findstring :,$(shell echo "${DST}" | sed 's/.*@//')))  # ':' after '@' is found
	$(eval BASE_PATH=$(shell echo "${DST}" | sed 's/.*@//' | sed 's/.*://'))
 endif
	@# Parsing BASE_PATH and TAG from DST (if 'upload-package'- or 'update-repository'-targets were found)
 ifneq (,$(findstring :/,$(shell echo "${DST}" | sed 's/.*@//')))  # ':/' after '@' is found
	$(eval BASE_PATH=$(shell echo "${DST}" | sed 's/.*@//' | sed 's/.*://'))
	$(eval TAG=)
 else
  ifneq (,$(findstring :,$(shell echo "${DST}" | sed 's/.*@//')))  # ':' after '@' is found
	$(eval TAG=$(shell echo "${DST}" | sed 's/.*@//' | sed 's/.*://'))
  endif
 endif
	@# Prints parsed variables and exits (target .parse-dst-argument is testing from external script)
 ifneq (,${CHECK_DST_ARGUMENT})
	@echo "${DST} => LOGIN=${LOGIN} PASSWORD=${PASSWORD} HOST=${HOST} BASE_PATH=${BASE_PATH} TAG=${TAG}"
	@exit 1
 endif
endif


.check-free-space-in-repository:
	@# Checks if there is enough space on the repository storage
	$(eval SRC_TOTAL_SIZE=$(shell du --bytes --total $^ | tail -n 1 | mawk '{print $$1}'))
	$(eval DST_FREE_SPACE=$(shell $(if ${PASSWORD},sshpass -p ${PASSWORD} )ssh $(or ${LOGIN},${DEFAULT_UPLOAD_PACKAGE_LOGIN})@${HOST} df -B1 $(or ${BASE_PATH},${DEFAULT_UPLOAD_PACKAGE_PATH}$(or ${TAG},${DEFAULT_TAG})) | tail -n 1 | mawk '{print $$4}'))
	@test ${SRC_TOTAL_SIZE} -gt ${DST_FREE_SPACE} \
		&& echo \
		&& echo "ERROR: There is not enough space on the repository storage (needed: ${SRC_TOTAL_SIZE} bytes, available: ${DST_FREE_SPACE} bytes)" \
		&& echo \
		&& exit 1 \
		|| exit 0 \
	;


## Runs tests.
ifndef OVERRIDE_CHECK_TARGET
check::
	@echo "\n========================================" && echo "Target \`$@\`:\n"

	@# Runs tests for python-scripts
	rm -rf /tmp/.pytest_cache
	echo "Running doc-tests and py-tests..." \
		&& python -B -m pytest -v -ocache_dir=/tmp/.pytest_cache -opython_files="*.py *.pyo" ${PYTEST_OPTS} $(shell ls -d */) \
		|| ( pytest_exitcode=$$? && echo Pytest exit code $${pytest_exitcode} \
		&& if [ "$${pytest_exitcode}" = "5" ]; then exit 0; else exit $${pytest_exitcode}; fi )

	@echo "Done."
endif


# Prints help messages
ifndef OVERRIDE_HELP_TARGET
help::
	@# Parses Makefile and prints doc-strings
	@mawk ' \
		BEGIN			{ \
							command = " \
								cat $(or ${HELP_MAKEFILE_LIST},${MAKEFILE_LIST}) \
									| grep \"^# Description:\ \" \
									| tail -n 1 \
									| sed -e \"s/# Description: \(.\)/\u\\1/\" \
									| tr -d \"\n\" \
							"; \
							command | getline RESULT; \
							close(command); \
							printf "%s:\n", RESULT; \
						} \
		/^$$/			{ delete message; count = 0; } \
		/^[a-zA-Z-]+:/	{ \
							sub(/:.*/, "", $$0); \
							if (count != 0) { \
								printf "\n%-24s", " * "$$0; \
								for (i = 1; i in message; i ++) { \
									printf "%s\n%24s", message[i], ""; \
								} \
							} \
							delete message; count = 0; \
						} \
		/^##.*$$/		{ \
							sub(/^## ?/, "", $$0); \
							count += 1; \
							message[count] = $$0; \
						} \
		END				{ printf "\n\n"; } \
		' $(or ${HELP_MAKEFILE_LIST},${MAKEFILE_LIST})
endif


## Removes temporary data.
ifndef OVERRIDE_CLEAN_TARGET
clean::
	(rm -rf \
		.build \
	)
endif
